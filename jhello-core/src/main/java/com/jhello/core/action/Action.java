package com.jhello.core.action;

import java.lang.reflect.Method;

import com.jhello.core.pub.HttpMethod;

public class Action {

	private String url;
	private Class<?> controllerCls;
	private Method method;
	private HttpMethod httpMethod;
	private Params params;
	
	public Action(String url, Class<?> controllerCls, Method method,HttpMethod httpMethod) {
		super();
		this.url = url;
		this.controllerCls = controllerCls;
		this.method = method;
		this.httpMethod = httpMethod;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Class<?> getControllerCls() {
		return controllerCls;
	}

	public void setControllerCls(Class<?> controllerCls) {
		this.controllerCls = controllerCls;
	}

	public Method getMethod() {
		return method;
	}

	public void setMethod(Method method) {
		this.method = method;
	}


	public Params getParams() {
		return params;
	}


	public void setParams(Params params) {
		this.params = params;
	}

	public HttpMethod getHttpMethod() {
		return httpMethod;
	}

	public void setHttpMethod(HttpMethod httpMethod) {
		this.httpMethod = httpMethod;
	}

	@Override
	public String toString() {
		return "Action [url=" + url + ", controllerCls=" + controllerCls
				+ ", method=" + method + ", httpMethod=" + httpMethod
				+ ", params=" + params + "]";
	}
	
	

}
