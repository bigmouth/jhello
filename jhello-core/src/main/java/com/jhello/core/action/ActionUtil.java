package com.jhello.core.action;

import com.jhello.core.pub.HttpMethod;

public class ActionUtil {

	public static String createActionMapperKey(String url,HttpMethod method){
		return String.format("%s#%s", method.name(),url);
	}
}
