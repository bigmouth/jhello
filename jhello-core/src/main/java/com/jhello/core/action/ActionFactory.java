package com.jhello.core.action;

import com.jhello.core.pub.HttpMethod;

public final class ActionFactory {

	private static ActionFactory _instance;
	private static Object _lock = new Object();
	private ActionFactory(){}
	
	public static ActionFactory getInstance(){
		if(_instance == null){
			synchronized (_lock) {
				if(_instance == null){
					_instance = new ActionFactory();
				}
			}
		}
		return _instance;
	}
	
	public Action getAction(String url,HttpMethod httpMethod) throws ClassNotFoundException,NoSuchMethodException, SecurityException{
		return ActionMapper.getInstance().getAction(url, httpMethod);
	}
	
}
