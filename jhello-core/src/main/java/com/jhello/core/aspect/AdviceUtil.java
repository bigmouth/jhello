package com.jhello.core.aspect;

import java.lang.reflect.Method;

public class AdviceUtil {

	/**
	 * 根据类获取符合切面匹配模式的字符串
	 * @param actionCls
	 * @param method
	 * @return
	 */
	public static String createPatternStr(Class<?> actionCls, Method method){
		return String.format("%s#%s", actionCls.getName(), method.getName());
	}
}
